Format: 1.0
Source: nebula-sshd-config
Binary: nebula-sshd-config
Architecture: all
Version: 0.1
Maintainer: Ernesto Nadir Crespo Avila <ecrespo@gmail.com>
Standards-Version: 3.9.2
Build-Depends: cdbs (>= 0.4.23-1.1), debhelper (>= 4.2.0), config-package-dev (>= 4.5~)
Package-List:
 nebula-sshd-config deb config extra arch=all
Checksums-Sha1:
 70f39da6ace587422c992a6a6a01d7b440694149 2855 nebula-sshd-config_0.1.tar.gz
Checksums-Sha256:
 f3315eaed279f30f0cac2a3410da1e51d37a554a3c6f824e268c37faa3ca406f 2855 nebula-sshd-config_0.1.tar.gz
Files:
 42f56cd73f2c2d4d6e66ad863201b395 2855 nebula-sshd-config_0.1.tar.gz
